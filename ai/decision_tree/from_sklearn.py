import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier, export_graphviz
from sklearn.model_selection import train_test_split
from pydotplus import graph_from_dot_data

sys.path.append('/home/alphapeas/my/kpi_labs/ai/')
from custom_utils import plot_decision_regions


if __name__ == "__main__":
    iris = datasets.load_iris()
    x = iris.data[:, [2, 3]]
    y = iris.target
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.3, random_state=2, stratify=y
    )
    sc = StandardScaler()
    sc.fit(x_train)
    x_train_std = sc.transform(x_train)
    x_test_std = sc.transform(x_test)

    x_combined_std = np.vstack((x_train_std, x_test_std))
    y_combined = np.hstack((y_train, y_test))

    tree = DecisionTreeClassifier(
        criterion="gini",
        max_depth=4,
        random_state=1,
    )
    tree.fit(x_train, y_train)
    plot_decision_regions(
        x_train,
        y_train,
        classifier=tree,
    )

    plt.xlabel("petal length [standartized]")
    plt.ylabel("petal width [standartized]")
    plt.legend(loc="upper left")
    #plt.show()

    dot_data = export_graphviz(
        tree,
        filled=True,
        rounded=True,
        class_names=["Setosa", "Versicola", "Virginica"],
        feature_names=["petal length", "petal width"],
        out_file=None
    )
    graph = graph_from_dot_data(dot_data)
    graph.write_png("tree.png")
