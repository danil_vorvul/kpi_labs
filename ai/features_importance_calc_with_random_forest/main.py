import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy.core.overrides import array_function_dispatch
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split


if __name__ == "__main__":
    df_wine = pd.read_csv(
        "https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data"
    )
    df_wine.columns = [
        "Метка класса",
        "Алкоголь",
        "Яблочная кислота",
        "Зола",
        "Щелочность золы",
        "Магний",
        "Всего фенолов",
        "Флавоноиды",
        "Нефлавоноидные фенолы",
        "Проантоцианидины",
        "Интенсивность цвета",
        "Оттенок",
        "OD280/OD315 разбавленных вин",
        "Пролин",
    ]

    feat_labels = df_wine.columns[1:]
    x, y = df_wine.iloc[:, 1:].values, df_wine.iloc[:, 0].values
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.3, random_state=0, stratify=y
    )
    forest = RandomForestClassifier(
        n_estimators=500,
        random_state=1
    )
    forest.fit(x_train, y_train)
    importances = forest.feature_importances_

    indices = np.argsort(importances)[::-1]

    for f in range(x_train.shape[1]):
        print("%2d %-*s %f" % (f + 1, 30, feat_labels[indices[f]], importances[indices[f]]))

    plt.title("Feature importance")
    plt.bar(
        range(x_train.shape[1]),
        importances[indices],
        align="center"
    )
    plt.xticks(
        range(x_train.shape[1]),
        feat_labels,
        rotation=90
    )
    plt.xlim([-1, x_train.shape[1]])
    plt.tight_layout()
    plt.show()