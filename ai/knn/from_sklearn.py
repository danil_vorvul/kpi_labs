import sys

import matplotlib.pyplot as plt
import numpy as np
from sklearn.neighbors import KNeighborsClassifier

sys.path.append("/home/alphapeas/my/kpi_labs/ai/")
from custom_utils import load_preprocessed_iris, plot_decision_regions

if __name__ == "__main__":
    (
        x_train,
        x_test,
        y_train,
        y_test,
        x_train_std,
        x_test_std,
        x_combined,
        y_combined,
    ) = load_preprocessed_iris()

    knn = KNeighborsClassifier(n_neighbors=5, p=2, metric="minkowski")
    knn.fit(x_train, y_train)
    plot_decision_regions(
        x_combined, y_combined, classifier=knn, test_idx=range(100, 150)
    )

    plt.xlabel("petal length [standartized]")
    plt.ylabel("petal width [standartized]")
    plt.legend(loc="upper left")
    plt.show()
