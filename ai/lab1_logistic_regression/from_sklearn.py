import sys
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from dataclasses import dataclass, field

from sklearn import datasets
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression


sys.path.append('/home/alphapeas/my/kpi_labs/ai/')
from custom_utils import plot_decision_regions


if __name__ == "__main__":
    iris = datasets.load_iris()
    x = iris.data[:, [2, 3]]
    y = iris.target
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.3, random_state=1, stratify=y
    )

    sc = StandardScaler()
    sc.fit(x_train)
    x_train_std = sc.transform(x_train)
    x_test_std = sc.transform(x_test)

    lr = LogisticRegression(C=100.0, random_state=1)
    lr.fit(x_train_std, y_train)
    plot_decision_regions(
        x_train_std,
        y_train,
        classifier=lr,
    )
    plt.xlabel("petal length [standartized]")
    plt.ylabel("petal width [standartized]")
    plt.legend(loc="upper left")
    plt.show()