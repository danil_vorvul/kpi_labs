import sys
import numpy as np
import matplotlib.pyplot as plt
from dataclasses import dataclass, field
from sklearn import datasets
from sklearn.model_selection import train_test_split

sys.path.append('/home/alphapeas/my/kpi_labs/ai/')
from custom_utils import plot_decision_regions


@dataclass
class LogisticRegression:
    eta: float = 0.05
    n_iter: int = 100
    random_state = 1
    w_: list = field(default_factory=list)
    cost_: list = field(default_factory=list)

    def fit(self, x, y):
        rgen = np.random.RandomState(self.random_state)
        self.w_ = rgen.normal(loc=0.0, scale=0.01, size=1 + x.shape[1])

        for _ in range(self.n_iter):
            net_input = self.net_input(x)
            output = self.activation(net_input)
            errors = y - output
            self.w_[1:] += self.eta * x.T.dot(errors)
            self.w_[0] += self.eta * errors.sum()

            cost = -y.dot(np.log(output)) - (1 - y).dot(np.log(1 - output))
            self.cost_.append(cost)

        return self

    def net_input(self, x):
        return np.dot(x, self.w_[1:]) + self.w_[0]

    def activation(self, z):
        return 1.0 / (1.0 + np.exp(-np.clip(z, -250, 250)))

    def predict(self, x):
        return np.where(self.net_input(x) >= 0.0, 1, 0)


if __name__ == "__main__":
    iris = datasets.load_iris()
    x = iris.data[:, [2, 3]]
    y = iris.target
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.3, random_state=1, stratify=y
    )

    x_train_01_subset = x_train[(y_train == 0) | (y_train == 1)]
    y_train_01_subset = y_train[(y_train == 0) | (y_train == 1)]

    lr = LogisticRegression()
    lr.fit(x_train_01_subset, y_train_01_subset)

    plot_decision_regions(x_train_01_subset, y_train_01_subset, classifier=lr)

    plt.xlabel("petal length [standartized]")
    plt.ylabel("petal width [standartized]")
    plt.legend(loc="upper left")
    plt.show()
