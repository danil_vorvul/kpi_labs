import sys
import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.tree import export_graphviz
from pydotplus import graph_from_dot_data

sys.path.append("/home/alphapeas/my/kpi_labs/ai/")
from custom_utils import plot_decision_regions, load_preprocessed_iris


if __name__ == "__main__":
    (
        x_train,
        x_test,
        y_train,
        y_test,
        x_train_std,
        x_test_std,
        x_combined,
        y_combined,
    ) = load_preprocessed_iris()

    forest = RandomForestClassifier(
        criterion="gini", n_estimators=100, random_state=1, n_jobs=4
    )
    forest.fit(x_train, y_train)
    plot_decision_regions(
        x_combined,
        y_combined,
        classifier=forest,
    )

    plt.xlabel("petal length [standartized]")
    plt.ylabel("petal width [standartized]")
    plt.legend(loc="upper left")
    plt.show()
