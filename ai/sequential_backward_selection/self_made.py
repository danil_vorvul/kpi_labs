import sys
from dataclasses import dataclass
from itertools import combinations

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn.base import clone
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier

sys.path.append("/home/alphapeas/my/kpi_labs/ai/")
from custom_utils import load_preprocessed_iris, plot_decision_regions


@dataclass
class SBS:
    estimator: KNeighborsClassifier
    k_features: int
    scoring = accuracy_score
    test_size: float = 0.25
    random_state: int = 1

    def fit(self, x, y):
        x_train, x_test, y_train, y_test = train_test_split(
            x, y, test_size=self.test_size, random_state=self.random_state
        )

        dim = x_train.shape[1]
        self.indices_ = tuple(range(dim))
        self.subsets_ = [self.indices_]
        score = self._calc_score(x_train, y_train, x_test, y_test, self.indices_)
        self.scores_ = [score]

        while dim > self.k_features:
            scores = []
            subsets = []
            for p in combinations(self.indices_, r=dim - 1):
                score = self._calc_score(x_train, y_train, x_test, y_test, p)
                scores.append(score)
                subsets.append(p)

            best = np.argmax(scores)
            self.indices_ = subsets[best]
            self.subsets_.append(self.indices_)
            dim -= 1

            self.scores_.append(scores[best])
        self.k_score_ = self.scores_[-1]

        return self

    def transform(self, x):
        return x[:, self.indices_]

    def _calc_score(self, x_train, y_train, x_test, y_test, indices):
        self.estimator.fit(x_train[:, indices], y_train)
        y_pred = self.estimator.predict(x_test[:, indices])
        score = self.scoring(y_test, y_pred)
        return score



if __name__ == "__main__":

    df_wine = pd.read_csv(
        "https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data"
    )
    df_wine.columns = [
        "Метка класса",
        "Алкоголь",
        "Яблочная кислота",
        "Зола",
        "Щелочность золы",
        "Магний",
        "Всего фенолов",
        "Флавоноиды",
        "Нефлавоноидные фенолы",
        "Проантоцианидины",
        "Интенсивность цвета",
        "Оттенок",
        "OD280/OD315 разбавленных вин",
        "Пролин",
    ]
    print("Метки классов", np.unique(df_wine["Метка класса"]))
    x, y = df_wine.iloc[:, 1:].values, df_wine.iloc[:, 0].values
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.3, random_state=0, stratify=y
    )
    # * Нормализация
    sc = StandardScaler()
    x_train_std = sc.fit_transform(x_train)
    x_test_std = sc.fit_transform(x_test)

    knn = KNeighborsClassifier(n_neighbors=5)

    sbs = SBS(estimator=knn, k_features=1)
    sbs.fit(x_train_std, y_train)