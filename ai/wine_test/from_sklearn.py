import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy.core.overrides import array_function_dispatch
import pandas as pd
from sklearn.model_selection import train_test_split

sys.path.append("/home/alphapeas/my/kpi_labs/ai/")
from custom_utils import load_preprocessed_iris, plot_decision_regions

if __name__ == "__main__":
    df_wine = pd.read_csv(
        "https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data"
    )
    df_wine.columns = [
        "Метка класса",
        "Алкоголь",
        "Яблочная кислота",
        "Зола",
        "Щелочность золы",
        "Магний",
        "Всего фенолов",
        "Флавоноиды",
        "Нефлавоноидные фенолы",
        "Проантоцианидины",
        "Интенсивность цвета",
        "Оттенок",
        "OD280/OD315 разбавленных вин",
        "Пролин",
    ]
    print("Метки классов", np.unique(df_wine["Метка класса"]))
    x, y = df_wine.iloc[:, 1:].values, df_wine.iloc[:, 0].values
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.3, random_state=0, stratify=y
    )
        # * Нормализация
    from sklearn.preprocessing import StandardScaler
    sc = StandardScaler()
    x_train_std = sc.fit_transform(x_train)
    x_test_std = sc.fit_transform(x_test)

    # * Нормализация по минимаксу
    from sklearn.preprocessing import MinMaxScaler
    mms = MinMaxScaler()
    x_train_norm = mms.fit_transform(x_train)
    x_test_norm = mms.fit_transform(x_test)

    # * Нормализация
    from sklearn.preprocessing import StandardScaler
    sc = StandardScaler()
    x_train_std = sc.fit_transform(x_train)
    x_test_std = sc.fit_transform(x_test)
    
    print("Default")
    print(x_test)
    print("Normalized")
    print(x_test_norm)
    print("Standrtized")
    print(x_test_std)