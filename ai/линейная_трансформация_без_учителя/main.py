import sys
import matplotlib.pyplot as plt
import numpy as np
from numpy.core.overrides import array_function_dispatch
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split


if __name__ == "__main__":
    df_wine = pd.read_csv(
        "https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data", header=None
    )
    # df_wine.columns = [
    #     "Метка класса",
    #     "Алкоголь",
    #     "Яблочная кислота",
    #     "Зола",
    #     "Щелочность золы",
    #     "Магний",
    #     "Всего фенолов",
    #     "Флавоноиды",
    #     "Нефлавоноидные фенолы",
    #     "Проантоцианидины",
    #     "Интенсивность цвета",
    #     "Оттенок",
    #     "OD280/OD315 разбавленных вин",
    #     "Пролин",
    # ]

    x, y = df_wine.iloc[:, 1:].values, df_wine.iloc[:, 0].values
    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.3, random_state=0, stratify=y
    )

    # Standatartize
    from sklearn.preprocessing import StandardScaler
    sc = StandardScaler()
    x_train_std = sc.fit_transform(x_train)
    x_test_std = sc.transform(x_test)
