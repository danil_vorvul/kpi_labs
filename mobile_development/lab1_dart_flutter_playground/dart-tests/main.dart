import "dart:math";
 
bool intToBool(int a) => a == 0 ? false : true;

// базовый класс еды
class Food
{
  int number_of_bites;
  int heal;
  int damage;

  // конструктор генерирует еду с рандомными свойствами
  //( может быть сьедобна и лечить или ядовита и наносить урон)
  Food([int number_of_bites=10]) {
    var rng = new Random();
    print("Конструктор класса Food\n");
    this.number_of_bites = number_of_bites;
   
    if(intToBool(rng.nextInt(1)))
    {
      this.heal = rng.nextInt(5);
      this.damage = 0;
    }
    else
    {
      this.damage = -1 * rng.nextInt(5);
      this.heal = 0;
    }
  }

  int bite()
  {
    int new_number_of_bites = this.number_of_bites - 1;
    
    if (new_number_of_bites == 0)
    {
      this.destruct();
    }
    else
    {
      this.number_of_bites = new_number_of_bites;
    }
      return this.damage + this.heal;
  }

  void destruct()
  {
    print("Сьедено");
  }

  @override
  String toString() {
    return "Number of bites - $number_of_bites, Heal - $heal";
  }

}

//Конкретно реализация яблока
class EdibleApple extends Food
{
  //наследуюсь от еды и добавляю параметры конкретные
  // для сьедобного яблока
  String _color;
  String _form;
  // переопределил конструктор, чтобы можно было указать
  //количество укусов до сьедания яблока и количество лечения
  EdibleApple([number_of_bites = 10, heal = 1, color = 'red', form = 'round'])
  {
    this.number_of_bites = number_of_bites;
    this.heal = heal;
    this._color = color;
    this._form = form;
    this.damage = 0;
    print("Лечение - ${this.heal}. Укусов - ${this.number_of_bites}. Урон - ${this.damage}.\n");
  }

  @override
  String toString() {
    return "Number of bites - $number_of_bites, Heal - $heal, Color - $_color";
  }
}


class Human
{
  int _heatPoints;
  
  int _maxHealth;
  
  Human(int heatPoints){
    this._heatPoints = heatPoints;
    this._maxHealth = heatPoints;
  }

  String toString(){
    return "HP - $_heatPoints, Max HP - $_maxHealth";
  }

  void eat(obj) {
    int change_points = obj.bite();
    change_points = this._heatPoints + change_points;
    if (change_points <= 0) {
      this.dead();
    }
    else if(change_points >= this._maxHealth)
    {
      this._heatPoints = this._maxHealth;
      print("Полный запас здоровья !");
    }
    else
    {
      print("Запас здоровья изменен.");
      this._heatPoints = change_points;
    }
    }

  void dead(){
    this.destruct();
  }

  void destruct() {
    print("Произошла смерть");
  }
}

class Customer {
  String name;
  int age;

  Customer(this.name, this.age);

  @override
  String toString() {
    return '{ ${this.name}, ${this.age} }';
  }
}

void main(){
  // General
  var g = 5;
  int a = 0;
  g = a;
  print(g);
  int b = 3;
  String c = "4";
  bool isPrivate = true;

  // Lists and maps
  var list = [];
  Map map = {'Jack': 23, 'Adam': 27, 'Katherin': 25};
  map.forEach((k, v) => list.add(Customer(k, v)));
  print(list);

  map.entries.forEach((e) => list.add(Customer(e.key, e.value)));
  print(list);

  var map2 = {};
  list.forEach((customer) => map2[customer.name] = customer.age);
  print(map2);

  // Strings
  String word = "word";
  print('$word'); 
  print('${word.toUpperCase()}');

  EdibleApple edibleApple = EdibleApple(10, 5);
  print(edibleApple);
  Human oleg = Human(25);
  print(oleg);
  oleg.eat(edibleApple);
  print(edibleApple);
}

