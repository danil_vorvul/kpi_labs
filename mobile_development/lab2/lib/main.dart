import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:lab2/views/settings.dart';
import 'package:lab2/views/posts.dart';
import 'package:lab2/services/theme_provider.dart';
import 'package:lab2/models/chat.dart';
import 'package:lab2/views/profile.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => ThemeNotifier(),
      child: Consumer<ThemeNotifier>(
        builder: (context, ThemeNotifier notifier, child) {
          return MaterialApp(
            title: "Flutter Provider",
            theme: notifier.darkTheme ? dark : light,
            home: TelegramHomePage(title: 'Telegram alpha 0.0.1'),
            routes: {
              '/settings': (context) => SettingsPage(),
              '/posts': (context) => PostsPage(),
              '/profile': (context) => UserProfile(),
            },
          );
        },
      ),
    );
  }
}

class TelegramHomePage extends StatefulWidget {
  final String title;

  TelegramHomePage({this.title = ""});

  @override
  _TelegramHomePageState createState() => _TelegramHomePageState();
}

class _TelegramHomePageState extends State<TelegramHomePage> {
  int taps = 0;
  void grow() {
    setState(() {
      this.taps += 1;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black12,
      appBar: AppBar(
        title: Text(this.taps.toString()),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: TelegramChats(),
      ),
      drawer: SettingsMenu(this.grow),
    );
  }
}

class SettingsMenu extends StatelessWidget {
  // const SettingsMenu({Key key}) : super(key: key);
  var _grow;
  SettingsMenu(gr) {
    this._grow = gr;
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Colors.black,
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: MyAvatarAndInfo(),
              decoration: BoxDecoration(
                color: Colors.black,
              ),
            ),
            ListTile(
              leading: Icon(
                Icons.group,
                color: Colors.white,
              ),
              hoverColor: Colors.black,
              title: Text(
                'New Group',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                return _grow();
              },
            ),
            ListTile(
              leading: Icon(
                Icons.lock,
                color: Colors.white,
              ),
              hoverColor: Colors.black,
              title: Text(
                'Item New Secret Chat',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.chat,
                color: Colors.white,
              ),
              hoverColor: Colors.black,
              title: Text(
                'New Channel',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.call,
                color: Colors.white,
              ),
              hoverColor: Colors.black,
              title: Text(
                'Posts',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                Navigator.pushNamed(context, '/posts');
              },
            ),
            ListTile(
              leading: Icon(
                Icons.save_outlined,
                color: Colors.white,
              ),
              hoverColor: Colors.black,
              title: Text(
                'Profile',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                Navigator.pushNamed(context, '/profile');
              },
            ),
            ListTile(
              leading: Icon(
                Icons.settings,
                color: Colors.white,
              ),
              hoverColor: Colors.black,
              title: Text(
                'Settings',
                style: TextStyle(color: Colors.white),
              ),
              // onTap: () => Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     builder: (context) => SettingsPage(),
              //   ),
              // ),
              onTap: () {
                Navigator.pushNamed(context, '/settings');
              },
            ),
            ListTile(
              leading: Icon(
                Icons.group_add_sharp,
                color: Colors.white,
              ),
              hoverColor: Colors.black,
              title: Text(
                'Invite friends',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(
                Icons.help_outline,
                color: Colors.white,
              ),
              hoverColor: Colors.black,
              title: Text(
                'Telegram FAQ',
                style: TextStyle(color: Colors.white),
              ),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}

class MyAvatarAndInfo extends StatelessWidget {
  const MyAvatarAndInfo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(flex: 3, child: UserAvatar()),
          Expanded(flex: 7, child: UserInfo())
        ],
      ),
    );
  }
}

class UserInfo extends StatelessWidget {
  const UserInfo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Danil",
            style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
          ),
          Text(
            "+3806601241254",
            style:
                TextStyle(fontWeight: FontWeight.bold, color: Colors.white70),
          ),
        ],
      ),
    );
  }
}

class TelegramChats extends StatelessWidget {
  const TelegramChats({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Map dialogs = DialogProvider.getDialogs();

    return Column(children: [
      Container(
          margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
          child: Row(
            children: [
              UserAvatar(
                  avatarUrl:
                      "https://media1.popsugar-assets.com/files/thumbor/zoJPi3bHw6mKrhhKqy69sjmpgeo/0x141:2397x2538/fit-in/500x500/filters:format_auto-!!-:strip_icc-!!-/2020/02/11/792/n/1922398/11f5609f5e42ebcb0c5a75.34705202_/i/Emily-Ratajkowski.jpg"),
              UsernameAndChatPreview(
                  username: "Арарат", chatPreview: "value.chatPreview"),
            ],
          )),
      Container(
          margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
          child: Row(
            children: [
              UserAvatar(
                  avatarUrl:
                      "https://media1.popsugar-assets.com/files/thumbor/zoJPi3bHw6mKrhhKqy69sjmpgeo/0x141:2397x2538/fit-in/500x500/filters:format_auto-!!-:strip_icc-!!-/2020/02/11/792/n/1922398/11f5609f5e42ebcb0c5a75.34705202_/i/Emily-Ratajkowski.jpg"),
              UsernameAndChatPreview(
                  username: "Арарат", chatPreview: "value.chatPreview"),
            ],
          )),
      Container(
          margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
          child: Row(
            children: [
              UserAvatar(
                  avatarUrl:
                      "https://media1.popsugar-assets.com/files/thumbor/zoJPi3bHw6mKrhhKqy69sjmpgeo/0x141:2397x2538/fit-in/500x500/filters:format_auto-!!-:strip_icc-!!-/2020/02/11/792/n/1922398/11f5609f5e42ebcb0c5a75.34705202_/i/Emily-Ratajkowski.jpg"),
              UsernameAndChatPreview(
                  username: "Арарат", chatPreview: "value.chatPreview"),
            ],
          )),
      Container(
          margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
          child: Row(
            children: [
              UserAvatar(
                  avatarUrl:
                      "https://media1.popsugar-assets.com/files/thumbor/zoJPi3bHw6mKrhhKqy69sjmpgeo/0x141:2397x2538/fit-in/500x500/filters:format_auto-!!-:strip_icc-!!-/2020/02/11/792/n/1922398/11f5609f5e42ebcb0c5a75.34705202_/i/Emily-Ratajkowski.jpg"),
              UsernameAndChatPreview(
                  username: "Арарат", chatPreview: "value.chatPreview"),
            ],
          )),
      Container(
          margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
          child: Row(
            children: [
              UserAvatar(
                  avatarUrl:
                      "https://media1.popsugar-assets.com/files/thumbor/zoJPi3bHw6mKrhhKqy69sjmpgeo/0x141:2397x2538/fit-in/500x500/filters:format_auto-!!-:strip_icc-!!-/2020/02/11/792/n/1922398/11f5609f5e42ebcb0c5a75.34705202_/i/Emily-Ratajkowski.jpg"),
              UsernameAndChatPreview(
                  username: "Арарат", chatPreview: "value.chatPreview"),
            ],
          )),
    ]);
  }
}

class Dialog extends StatelessWidget {
  String username = "aaa";
  String chatPreview = "bbbb";
  String avatarUrl =
      "https://media1.popsugar-assets.com/files/thumbor/zoJPi3bHw6mKrhhKqy69sjmpgeo/0x141:2397x2538/fit-in/500x500/filters:format_auto-!!-:strip_icc-!!-/2020/02/11/792/n/1922398/11f5609f5e42ebcb0c5a75.34705202_/i/Emily-Ratajkowski.jpg";
  Dialog({String avatarUrl, String username, String chatPreview});
  // const Dialog({Key key, String avatarUrl, String username, String chatPreview}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<DialogModel>(
      builder: (context, value, child) => Container(
          margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
          child: Row(
            children: [
              UserAvatar(avatarUrl: value.avatarUrl),
              UsernameAndChatPreview(
                  username: value.username, chatPreview: value.chatPreview),
            ],
          )),
    );
  }
}

class UsernameAndChatPreview extends StatelessWidget {
  String username = "Alina Ratajkowski";
  String chatPreview =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit.";
  // const UsernameAndChatPreview({Key key, String username, String chatPreview}) : super(key: key);
  UsernameAndChatPreview({String username, String chatPreview});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        child:
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
          Expanded(
            child: Container(
              height: 70,
              margin: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                        child: Text(
                      this.username,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white70),
                    )),
                  ),
                  Expanded(child: Container(), flex: 1),
                  Expanded(
                    flex: 3,
                    child: Container(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                        child: Text(
                          this.chatPreview,
                          overflow: TextOverflow.fade,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white70),
                        )),
                  )
                ],
              ),
            ),
          ),
          Row(children: [
            Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.white),
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
              ),
              margin: EdgeInsets.symmetric(horizontal: 15.0, vertical: 20.0),
              padding: EdgeInsets.symmetric(horizontal: 5, vertical: 5),
              child: Icon(
                Icons.push_pin_rounded,
                color: Colors.white,
              ),
            ),
          ]),
        ]),
      ),
    );
  }
}

class UserAvatar extends StatelessWidget {
  final String avatarUrl =
      "https://media1.popsugar-assets.com/files/thumbor/zoJPi3bHw6mKrhhKqy69sjmpgeo/0x141:2397x2538/fit-in/500x500/filters:format_auto-!!-:strip_icc-!!-/2020/02/11/792/n/1922398/11f5609f5e42ebcb0c5a75.34705202_/i/Emily-Ratajkowski.jpg";
  const UserAvatar({Key key, String avatarUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(50.0),
      child: Image.network(
        this.avatarUrl,
        height: 80.0,
        width: 80.0,
      ),
    );
  }
}
